package com.gen_game.exam.genesis.util;

public class CallProcessSimulator {
	/**
	 * Simulate call success probability
	 * Call is likely to success in 2/3 : 1/3 ratio
	 * @return true - call is successful<br/>
	 * false - call failed
	 */
	public static boolean isCallSuccessful(){
		return Math.random() > 0.3;
	}
}
