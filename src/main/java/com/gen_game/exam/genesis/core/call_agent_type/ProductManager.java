package com.gen_game.exam.genesis.core.call_agent_type;

import com.gen_game.exam.genesis.core.Call;
import com.gen_game.exam.genesis.core.CallAgent;

public class ProductManager extends CallAgent{

	private static final String PROD_MANAGER_NAME ="Mangulabnan";
	
	private boolean available;
	/*
	 * Private constructor to avoid creation of new instance of this class
	 * outside of this class
	 */
	private ProductManager(String employeeName) {
		super(employeeName);
	}
	/**
	 * static class holder for our singleton class
	 * @author hostlocal
	 *
	 */
	private static class SingletonHelper {
		private static final ProductManager INSTANCE = new ProductManager(PROD_MANAGER_NAME);
	}
	/**
	 * Gets the instance of the singleton
	 * @return
	 */
	public static ProductManager getInstance() {
		return SingletonHelper.INSTANCE;
	}

	@Override
	protected void processCall(Call call) {
		System.out.println(call.getCallId() + " is done.");
	}

	@Override
	protected void escalate(Call call) {
		//lets just assume for now that there is no higher level
		//than the Product Manager, thus we do nothing here
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	

}
