package com.gen_game.exam.genesis.core.call_agent_type;

import java.util.concurrent.BlockingQueue;

import com.gen_game.exam.genesis.core.Call;
import com.gen_game.exam.genesis.core.CallAgent;
import com.gen_game.exam.genesis.util.CallProcessSimulator;

public class Fresher extends CallAgent {

	private BlockingQueue<Fresher> availableFresherQueue;
	
	public Fresher(String employeeName,BlockingQueue<Fresher> availableFresherQueue) {
		super(employeeName);
		this.availableFresherQueue = availableFresherQueue;
	}

	@Override
	protected void processCall(Call call) {
		if(CallProcessSimulator.isCallSuccessful()){
			System.out.println(call.getCallId() + " is done.");
		}else{
			escalate(call);
		}
		availableFresherQueue.add(this);
	}

	@Override
	protected void escalate(Call call) {
		System.out.println(call.getCallId() + " is escalated to TL");
	}

}
