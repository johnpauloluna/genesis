package com.gen_game.exam.genesis.core.call_agent_type;

import com.gen_game.exam.genesis.core.Call;
import com.gen_game.exam.genesis.core.CallAgent;
import com.gen_game.exam.genesis.util.CallProcessSimulator;

public class TechLead extends CallAgent {

	private static final String TECH_LEAD_NAME ="Hernando";
	
	private boolean available;
	/*
	 * Private constructor to avoid creation of new instance of this class
	 * outside of this class
	 */
	private TechLead(String employeeName) {
		super(employeeName);
	}
	/**
	 * static class holder for our singleton class
	 * @author hostlocal
	 *
	 */
	private static class SingletonHelper {
		private static final TechLead INSTANCE = new TechLead(TECH_LEAD_NAME);
	}
	/**
	 * Gets the instance of the singleton
	 * @return Singleton instance of this class
	 */
	public static TechLead getInstance() {
		return SingletonHelper.INSTANCE;
	}
	
	@Override
	protected void processCall(Call call) {
		if(CallProcessSimulator.isCallSuccessful() && this.isAvailable()){
			System.out.println(call.getCallId() + " is done.");
		}else{
			escalate(call);
		}
	}

	@Override
	protected void escalate(Call call) {
		System.out.println(call.getCallId() + " is escalated to PM.");
	}

	/**
	 * Check if Technical Lead is available for other calls
	 * @return
	 */
	public boolean isAvailable() {
		return available;
	}
	/**
	 * Sets tect lead availability
	 * @param available
	 */
	public void setAvailable(boolean available) {
		this.available = available;
	}


}
