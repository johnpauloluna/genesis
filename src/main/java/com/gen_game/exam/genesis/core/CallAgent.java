package com.gen_game.exam.genesis.core;

public abstract class CallAgent {
	
	private String name;
	
	public CallAgent(String employeeName) {
		this.name = employeeName;
	}
	/**
	 * Processes the call.
	 * @param call - call to be processed
	 */
	protected abstract void processCall(Call call);
	/**
	 * Escalates call. If call escalated from Fresher, call will be escalated to TL.<br/>
	 * Otherwise if call came from TL, call will be escalated to PM
	 * @param call - Call to be escalated
	 */
	protected abstract void escalate(Call call);
	/**
	 * Get call agent name
	 * @return call agent name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Sets call agent name
	 * @param name - call agent name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
