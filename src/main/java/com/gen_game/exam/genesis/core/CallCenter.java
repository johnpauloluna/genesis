package com.gen_game.exam.genesis.core;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.gen_game.exam.genesis.core.call_agent_type.Fresher;
import com.gen_game.exam.genesis.core.call_agent_type.ProductManager;
import com.gen_game.exam.genesis.core.call_agent_type.TechLead;
/**
 * Object representation of Call Center.
 * @author hostlocal
 *
 */
public class CallCenter {

	// only allow 20 total waiting and active call
	private BlockingQueue<Call> callQueue = new ArrayBlockingQueue<Call>(20);
	private BlockingQueue<Fresher> availableFresher;
	/**
	 * Create new instance of CallCenter and initiates availableFresher property with
	 * the passed parameter
	 * @param availableFresher
	 */
	public CallCenter(BlockingQueue<Fresher> availableFresher) {
		this.availableFresher = availableFresher;
		
	}
	/**
	 * Accepts a call and added to the call queue;
	 * @param call - Call to be added to call queue
	 */
	public void acceptCall(Call call) {
		if (callQueue.remainingCapacity() > 1) {
			callQueue.add(call);
		} else {
			// TODO: add function for letting caller know that
			// his/her call can't be accommodated
		}
	}
	/**
	 * Starts executing the calls in queue.
	 * 
	 */
	public void processCalls() {
		ExecutorService service = Executors.newFixedThreadPool(20);
		while (!callQueue.isEmpty()) {
			Call call = null;
			try {
				call = callQueue.take();
				setAvailableAgent(call);
				service.execute(call);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}

		}
		service.shutdown();
	}

	/**
	 * Checks for available CallAgent
	 * If no Fresher,TL or PM is available,call will wait for the available Fresher. 
	 * @param call
	 * @throws InterruptedException
	 */
	private void setAvailableAgent(Call call) throws InterruptedException{
		if (availableFresher.isEmpty()) {
			if (TechLead.getInstance().isAvailable()) {
				call.setCallAgent(TechLead.getInstance());
			}else if(ProductManager.getInstance().isAvailable()){
				call.setCallAgent(ProductManager.getInstance());
			}else{
				call.setCallAgent(availableFresher.take());
			}
		} else {
			call.setCallAgent(availableFresher.take());
		}
	}
}
