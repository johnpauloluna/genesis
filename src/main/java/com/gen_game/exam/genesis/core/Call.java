package com.gen_game.exam.genesis.core;

public class Call implements Runnable {

	private CallAgent callAgent;
	private String callId;
	/**
	 * Creates new Call instance.
	 * @param callId - initiates callId property with this parameter
	 */
	public Call(String callId) {
		this.callId = callId;
	}

	public void run() {
		System.out.println(getCallId()+" has started.\nAssigned to : "+getCallAgent().getName());
		callAgent.processCall(this);
	}
	/**
	 * Get CallAgent that handles this call
	 * @return
	 */
	public CallAgent getCallAgent() {
		return callAgent;
	}
	/**
	 * Sets the CallAgent/Employee that will handle this call
	 * @param callAgent
	 */
	public void setCallAgent(CallAgent callAgent) {
		this.callAgent = callAgent;
	}
	/**
	 * Get this calls ID
	 * @return callID
	 */
	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

}
