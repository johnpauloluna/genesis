package com.gen_game.exam.genesis.mock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.gen_game.exam.genesis.core.Call;
import com.gen_game.exam.genesis.core.call_agent_type.Fresher;

public class MockData {
	
	public static List<Call> createMockCalls() {
		List<Call> calls = new ArrayList<Call>();
		calls.add(new Call("Call-1"));
		calls.add(new Call("Call-2"));
		calls.add(new Call("Call-3"));
		calls.add(new Call("Call-4"));
		calls.add(new Call("Call-5"));
		calls.add(new Call("Call-6"));
		calls.add(new Call("Call-7"));
		calls.add(new Call("Call-8"));
		calls.add(new Call("Call-9"));
		calls.add(new Call("Call-10"));
		calls.add(new Call("Call-11"));
		calls.add(new Call("Call-12"));
		calls.add(new Call("Call-13"));
		calls.add(new Call("Call-14"));
		return calls;
	}

	private static List<Fresher> createMockFreshers(BlockingQueue<Fresher> availableFresherQueue) {
		List<Fresher> freshers = new ArrayList<Fresher>();
		freshers.add(new Fresher("Fresher-1",availableFresherQueue));
		freshers.add(new Fresher("Fresher-2",availableFresherQueue));
		freshers.add(new Fresher("Fresher-3",availableFresherQueue));
		freshers.add(new Fresher("Fresher-4",availableFresherQueue));
		freshers.add(new Fresher("Fresher-5",availableFresherQueue));
		return freshers;
	}
	
	public static BlockingQueue<Fresher> createMockFresherQueue(){
		BlockingQueue<Fresher> availableFresherQueue = new ArrayBlockingQueue<Fresher>(5);
		List<Fresher> freshers = createMockFreshers(availableFresherQueue);
		for (Fresher fresher : freshers) {
			if(availableFresherQueue.remainingCapacity() == 0){
				break;
			}
			availableFresherQueue.add(fresher);
		}
		
		return availableFresherQueue;
	}
}
