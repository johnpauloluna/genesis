package com.gen_game.exam.genesis;

import com.gen_game.exam.genesis.core.Call;
import com.gen_game.exam.genesis.core.CallCenter;
import com.gen_game.exam.genesis.mock.MockData;

/**
 * Hello world!
 *
 */
public class App {
	
	public static void main(String[] args) throws InterruptedException {

		CallCenter callCenter = new CallCenter(MockData.createMockFresherQueue());
		for (Call call : MockData.createMockCalls()) {
			callCenter.acceptCall(call);
		}

		callCenter.processCalls();
		
		
	}

}
